const data = $fuwuProxy()
data.spaces = []
data.agents = {}

let spacesMap = {}

async function dataChanged(){
    const newStorageData = await browser.storage.local.get()
    if (JSON.stringify(newStorageData?.spaces ?? []) != JSON.stringify(data.spaces)){
        data.spaces = newStorageData.spaces
    }

    if (JSON.stringify(newStorageData?.agents ?? {}) != JSON.stringify(data.agents)){
        data.agents = newStorageData.agents
    }
}

browser.storage.local.onChanged.addListener(dataChanged)

async function sessionChanged(){
    spacesMap = await browser.storage.session.get() ?? {}
}

browser.storage.session.onChanged.addListener(sessionChanged)

async function startUp(){
    await dataChanged()
    await sessionChanged()
    display.page = PAGES.HOME

    data.$listeners.push(() => {
        const dataClone = {}
        Object.keys(data).forEach((key) => {
            if (!key.startsWith("$")){
                dataClone[key] = data[key]
            }
        })
        const toSet = JSON.parse(JSON.stringify(dataClone))
        browser.storage.local.set(toSet);
    })
}


const PAGES = {
    WAITING: -1,
    HOME: 0,
    EDIT: 1,
    ADVANCED: 2
}

const display = $fuwuProxy()
display.page = PAGES.WAITING
display.inputName = ""
display.inputUrl = ""
display.icon = "loading.svg"
display.currentUid = ""

display.agentSelect = ""
display.agentInput = ""

startUp()


function createNewSpace(){
    display.page = PAGES.EDIT
    display.inputName = ""
    display.inputUrl = ""
    display.icon = "loading.svg"
    display.currentUid = ""
}

function editSpace(space){
    display.page = PAGES.EDIT
    display.inputName = space.name
    display.inputUrl = space.url
    display.icon = space.icon
    display.currentUid = space.uid
}

function deleteSpace(space){
    data.spaces = data.spaces.filter(s => s.uid != space.uid)
    browser.spaces.remove(spacesMap[space.uid])
}

async function submitEdit(){
    display.page = PAGES.HOME
    if (display.currentUid == ""){
        const space = {
            uid: crypto.randomUUID().replaceAll("-", "_"),
            name: display.inputName,
            url: getValidInputUrl(),
            icon: display.icon
        }
        data.spaces.push(space)

        const sp = await browser.spaces.create(space.uid, space.url, {
            defaultIcons: space.icon,
            title: space.name
        })
        spacesMap[space.uid] = sp.id
        browser.storage.session.set(spacesMap);
    } else {
        const space = data.spaces.find(s => s.uid == display.currentUid)
        space.name = display.inputName
        space.url = getValidInputUrl()
        space.icon = display.icon

        browser.spaces.update(spacesMap[space.uid], space.url, {
            defaultIcons: space.icon,
            title: space.name
        })
    }
}

async function detectIcon(){
    display.icon = "loading.svg"
    const href = getValidInputUrl()
    let icon = await getFavicon(href)
    if (icon == null){
        display.icon = "error.svg"
    } else {
        display.icon = icon
    }
}

async function selectFile(ev){
    if (ev.target.files.length > 0){
        const file = ev.target.files[0]
        const icon = await blobToDataUrl(file)
        if (icon == null){
            display.icon = "error.svg"
        } else {
            display.icon = icon
        }
    }
}

function isValidInputUrl(){
    const url = display.inputUrl
    return url.length > 0 && (isValidUrl(url) || isValidUrl("http://" + url))
}

function isValidUrl(urlString){
    try { 
        return Boolean(new URL(urlString)); 
    }
    catch(_){ 
        return false; 
    }
}

function getValidInputUrl(){
    try { 
        return new URL(display.inputUrl).href;
    }
    catch(_){ 
        return new URL("http://" + display.inputUrl).href;
    }
}

async function getFavicon(href){
    const url = new URL(href)
    //Untracked free api. Mozilla fans will like this more than some random api
    //Only works inside Thunderbird because CORS is unchecked here. In FF it would fail
    const duckDuckApi = `https://icons.duckduckgo.com/ip3/${url.hostname}.ico` 

    try {

        const favicon = await fetch(duckDuckApi)
        const favBlob = await favicon.blob()

        const dataUrl = await blobToDataUrl(favBlob)

        return dataUrl

    } catch (_){ //Something could have gone wrong in blob conversion or in the get
        return null
    }

}

function blobToDataUrl(blob) {
    return new Promise(function(resolve, reject) {
        const reader = new FileReader();

        reader.onloadend = () =>  resolve(reader.result);
        reader.onerror = () => reject()

        reader.readAsDataURL(blob);
    });
}


//Advanced page
function advancedSpace(space){
    display.page = PAGES.ADVANCED
    display.currentUid = space.uid
    display.inputName = space.name
    display.icon = space.icon
    display.url = space.url

    const agent = data.agents[space.uid] ?? ""

    display.agentInput = ""
    display.agentSelect = ""

    if (agent == "ff"){
        display.agentSelect = "ff"
    } else if (agent.length > 0) {
        display.agentSelect = "custom"
        display.agentInput = agent
    }
}

function mightSelect(option){
    if (option.value == display.agentSelect){
        option.setAttribute("selected", "")
    }
}

function applyAdvanced(){
    if (display.agentSelect == ""){
        delete data.agents[display.currentUid]
    } else if (display.agentSelect == "ff"){
        data.agents[display.currentUid] = "ff"
    } else {
        data.agents[display.currentUid] = display.agentInput
    }

    display.page = PAGES.HOME
}