[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/Z8Z7AO1BI)

# ThunderSuite

ThunderSuite is a WebExtension for Thunderbird that allows you to turn Thunderbird into a Internet Suite tailored to what you need, by allowing you to add as many "Spaces" to your Thunderbird sidebar as you need.

## Building (for addon reviewers)

All JS Code is unminified except
fuwu-pl.min.js which is a minimal framework taken from https://gitlab.com/DaGammla/fuwu.js

fuwu-pl.min.js is minified via uglify-js and originally looks like this:
https://gitlab.com/DaGammla/fuwu.js/-/blob/main/0.8.8/fuwu-pl.js

The build script for that repository is located at:
https://gitlab.com/DaGammla/fuwu.js/-/blob/main/create.js

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
