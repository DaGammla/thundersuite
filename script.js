const spacesMap = {}

async function spaceButton(space){
    const sp = await browser.spaces.create(space.uid, space.url, {
        defaultIcons: space.icon,
        title: space.name
    })
    spacesMap[sp.id] = space.uid
}

async function setupSpaces(){

    let data = { spaces: [] }
    try {
        data = await browser.storage.local.get() ?? data
    } catch { }
    
    data.spaces = data.spaces ?? []

    for (const space of data.spaces) {
        await spaceButton(space)
    }

    browser.storage.session.set(spacesMap)

}

window.setTimeout(() => setupSpaces().then(() => retrieveData()).then(() => makeUserAgentListener()))

let data = { spaces: [], agents: {} }

browser.storage.local.onChanged.addListener(async () => {
    await retrieveData()
    makeUserAgentListener()
})

async function retrieveData(){
    data = (await browser.storage.local.get()) ?? data
}

function makeUserAgentListener(){
    browser.webRequest.onBeforeSendHeaders.removeListener(userAgendAdjuster)

    if ((data?.spaces ?? []).length > 0){

        const urls = data.spaces.map(it => {
            const url = new URL("../*", it.url)
            url.protocol = "https"
            return url.href.replace("https://", "*://")
        })

        browser.webRequest.onBeforeSendHeaders.addListener(
            userAgendAdjuster,
            { urls: urls },
            ["blocking", "requestHeaders"]
        );
    }
}

function userAgendAdjuster(event){

    const siteUrl = new URL("../*", event.url)
    const matchAgainst = siteUrl.host + siteUrl.pathname + siteUrl.search

    let newAgent = null

    for (const space of data.spaces) {
        const spaceUrl = new URL("../", space.url)
        const compare = spaceUrl.host + spaceUrl.pathname + spaceUrl.search
        const agent = data.agents[space.uid] ?? ""

        if (agent != "" && matchAgainst.startsWith(compare)){
            newAgent = agent
            break
        }
    }


    if (newAgent != null) {
        for (const header of event.requestHeaders){
            if (header.name.toLowerCase() === "user-agent") {
                if (newAgent == "ff"){
                    header.value = header.value.replace("Thunderbird", "Firefox")
                } else {
                    header.value = newAgent
                }
                break
            }
        }
    }

    return { requestHeaders: event.requestHeaders };
}

